#!/bin/sh
# ff-security-update.sh - A script to automate rebuilds of gecko-dependent packages
#
# Copyright (C) 2007-2011 Red Hat, Inc.
# Author(s): Christopher Aillon <caillon@redhat.com>
#            Jan Horak <jhorak@redhat.com>
# 
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.
#

if [ -z "$1" ]; then
    echo "Usage: ff-security-update.sh -b fXX/master old-ver new-ver"
exit
fi
branch=
while getopts 'b:h' OPTION
do
	case $OPTION in
	b)
		branch=$OPTARG
		;;
	h)
		printf "Usage: %s -b <branch> <old-gecko> <new-gecko>\n" $(basename $0) >&2
		;;
	esac
done
shift $(($OPTIND - 1))
F12="mozvoikko gnome-web-photo perl-Gtk2-MozEmbed gnome-python2-extras galeon" #7
F13=$F12
F14=$F12
devel=$F12

OLDGECKO=`echo $1|sed -e 's/\./\\\./g'`
NEWGECKO=$2
packages=
if [ $branch = "f12" ]; then
	packages=$F12
elif [ $branch = "f13" ]; then
	packages=$F13
elif [ $branch = "f14" ]; then
	packages=$F13
elif [ $branch = "master" ]; then
	packages=$devel
else
	printf "Unknown branch: %s\n" $branch
	exit 1;
fi

saved_dir=`pwd`
cd ~/sources/fedoraproject.org
#rm -rf $packages
for package in $packages; do
   fedpkg clone $package -B
done
#cvs -d$CVSEXTRAS co $packages
echo "Just did cvs checkout"
for package in $packages; do
	cd ~/sources/fedoraproject.org

	# Get a clean and latest version
	#rm -rf $package
	#cvs -d$CVSEXTRAS co $package
	#echo "Just did cvs checkout"

	cd $package/$branch
	#sed -i -e "s/\<$OLDGECKO\>/$NEWGECKO/" ${package}.spec
	sed '/%changelog/,//d' ${package}.spec > ${package}.spec.new
	sed -i -e "s/\b$OLDGECKO[ \t]*$/$NEWGECKO/g" ${package}.spec.new
	echo "%changelog" >> ${package}.spec.new
	sed '1,/%changelog/d' ${package}.spec >> ${package}.spec.new
	mv ${package}.spec.new ${package}.spec
	if [ $branch = "F-8" ]; then
		sed -i -e "s/$OLDFF/$NEWFF/" ${package}.spec
	fi
	#echo "Just ran sed for version updates"
	rpmdev-bumpspec -c "- Rebuild against newer gecko" ${package}.spec
	#echo "Just did bumpspecfile"
	fedpkg clog
	echo "Review changes:"
	echo "-----------------------------------------------------------------------"
	git diff
	echo "-----------------------------------------------------------------------"
	echo "Is it ok to commit [Y/N]? "; read ask
	if [ $ask == 'y' ]; then
		echo "Commit..."
	else
		echo "Skipping package: $package"
		continue
	fi
	#echo "Just made clog"
	fedpkg commit -F clog -p
	fedpkg build --nowait
	cd $saved_dir
done
rm -rf $packages
